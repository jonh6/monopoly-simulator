# Monopoly Simulator

*Make sure that commons-csv-1.8.jar is in /lib. If not, download at [Apache Commons CSV](https://commons.apache.org/proper/commons-csv/download_csv.cgi)*


**Monopoly Simulator, capable of simulating entire monopoly games**

***Game modes:***
*  Simulate game from start with specified number of turns => Calculates %chance of landing on a specified space
*  Simulate full game from start => shows various game stats (not implemented yet)
*  Simulate game from specified point => Estimate winner from fixed situation (all properties sold), or estimate possible game outcomes by continuing game w/ AI (not implemented yet)
  

***AI Modes: (not implemented)***
* Aggressive: Buys all properties, even if mortages are needed, buys houses / hotels ASAP.
* Regular: Buys all properties as long as no mortgages are needed, buys houses w/ enough funds to pay other players
* Easy: Buys some properties, favors ones which lead to Monopolies, buys houses  w/ more than enough funds to pay other players


Stats print out to Stats.csv in project folder. Move after each game, or file will be overwritten

**Most up to date branch is currently Release-0.2, with PropertyManager under development**
