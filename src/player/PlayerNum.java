package player;

public enum PlayerNum {
    BANK(0),
    PLAYER_1(1),
    PLAYER_2(2),
    PLAYER_3(3),
    PLAYER_4(4),
    PLAYER_5(5),
    PLAYER_6(6);
    private int playerNum;

    PlayerNum(int playerNum) {
        this.playerNum = playerNum;
    }
}
