package game;

import card.Card;
import card.CardManager;

import java.util.Arrays;


public class Game {
    private Board board;
    private CardManager cardManager;

    public Game(){
        board = new Board();
        cardManager = new CardManager();
        board.createCells();
        board.listCells();

        for (int i = 0; i < 12; i++){
            System.out.println(cardManager.getNextChanceCard());
        }

        Dice dice = new Dice();
        for (int i = 0; i < 5; i++){
            System.out.println(Arrays.toString(dice.roll()));

        }
    }

    public void newGame(){

    }

    private void initialize(){

    }
}
