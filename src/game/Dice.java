package game;

import java.util.Random;

public class Dice {
    private static final int MIN = 1;
    private static final int MAX = 6;

    private Random rnd = new Random();
    public Dice() {
    }


    public int [] roll(){

        int d1 = rnd.nextInt(MAX) + MIN;
        int d2 = rnd.nextInt(MAX) + MIN;
        return new int[] {d1, d2};
    }
}
