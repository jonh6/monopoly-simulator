package game;

import cell.*;

import java.util.HashMap;

public class Board {
    HashMap<Integer, Cell> cellMap = new HashMap<>();

    public Board() {
    }

    public void createCells(){
        createPropertyCells();
        createUtilityAndRailroadsCells();
        createFeeCells();
        createOtherCells();
        createChanceAndCommunityCells();
    }

    private void addCell(Cell cell){
        cellMap.put(cell.getCellNum(), cell);
    }

    private void createPropertyCells(){
        createFirstSideProperty();
        createSecondSideProperty();
        createThirdSideProperty();
        createFourthSideProperty();
    }

    private void createFirstSideProperty(){
        int houseCost = 50;
        createBrownProperty(60, 2, houseCost, PropertyColor.BROWN);
        createLightBlueProperty(100, 6, houseCost, PropertyColor.LIGHT_BLUE);
    }

    private void createSecondSideProperty(){
        int houseCost = 100;
        createPinkProperty(140, 10, houseCost, PropertyColor.PINK);
        createOrangeProperty(180, 14, houseCost, PropertyColor.ORANGE);
    }

    private void createThirdSideProperty(){
        int houseCost = 150;
        createRedPropety(220, 18, houseCost, PropertyColor.RED);
        createYellowProperty(260, 22, houseCost, PropertyColor.YELLOW);
    }

    private void createFourthSideProperty(){
        int houseCost = 200;
        createGreenProperty(300, 26, houseCost, PropertyColor.GREEN);
        createDarkBlueProperty(350, 35, houseCost, PropertyColor.DARK_BLUE);
    }

    private void createBrownProperty(int cost, int rent, int houseCost, PropertyColor color){

        int rentHouses [] = {10, 30, 90, 160, 250};
        addPropertyToMap(1, "Mediterranean Avenue", color, rent, rentHouses, houseCost, cost);

        rentHouses = new int[] {20, 60, 180, 320, 450};
        addPropertyToMap(3, "Baltic Avenue", color, rent+2, rentHouses, houseCost, cost);
    }

    private void createLightBlueProperty(int cost, int rent, int houseCost, PropertyColor color){
        int rentHouses [] = {30, 90, 270, 400, 550};
        addPropertyToMap(6, "Oriental Avenue", color, rent, rentHouses, houseCost, cost);
        addPropertyToMap(8, "Vermont Avenue", color, rent, rentHouses, houseCost, cost);

        rentHouses = new int[] {40, 100, 300, 450, 600};
        addPropertyToMap(9, "Connecticut Avenue", color, rent+2, rentHouses, houseCost, cost+20);
    }

    private void createPinkProperty(int cost, int rent, int houseCost, PropertyColor color){
        int rentHouses [] = {50, 150, 450, 625, 750};
        addPropertyToMap(11, "St. Charles Place", color, rent, rentHouses, houseCost, cost);
        addPropertyToMap(13, "States Avenue", color, rent, rentHouses, houseCost, cost);

        rentHouses = new int[] {60, 180, 500, 700, 900};
        addPropertyToMap(14, "Virginia Avenue", color, rent+2, rentHouses, houseCost, cost + 20);
    }

    private void createOrangeProperty(int cost, int rent, int houseCost, PropertyColor color){
        int rentHouses [] = {70, 200, 550, 750, 950};
        addPropertyToMap(16, "St. James Place", color, rent, rentHouses, houseCost, cost);
        addPropertyToMap(18, "Tennesse Avenue", color, rent, rentHouses, houseCost, cost);

        rentHouses = new int[] {80, 220, 600, 800, 1000};
        addPropertyToMap(19, "New York Avenue", color, rent+2, rentHouses, houseCost, cost + 20);
    }

    private void createRedPropety(int cost, int rent, int houseCost, PropertyColor color){
        int rentHouses [] = {90, 250, 700, 875, 1050};
        addPropertyToMap(21, "Kentucky Avenue", color, rent, rentHouses, houseCost, cost);
        addPropertyToMap(23, "Indiana Avenue", color, rent, rentHouses, houseCost, cost);

        rentHouses = new int[] {100, 300, 750, 925, 1100};
        addPropertyToMap(24, "Illinois Avenue", color, rent+2, rentHouses, houseCost, cost + 20);
    }

    private void createYellowProperty(int cost, int rent, int houseCost, PropertyColor color){
        int rentHouses [] = {110, 330, 800, 975, 1150};
        addPropertyToMap(26, "Atlantic Avenue", color, rent, rentHouses, houseCost, cost);
        addPropertyToMap(27, "Ventor Avenue", color, rent, rentHouses, houseCost, cost);

        rentHouses = new int[] {120, 360, 850, 1025, 1200};
        addPropertyToMap(29, "Marvin Gardens", color, rent+2, rentHouses, houseCost, cost + 20);
    }

    private void createGreenProperty(int cost, int rent, int houseCost, PropertyColor color){
        int rentHouses [] = {130, 390, 900, 1100, 1275};
        addPropertyToMap(31, "Pacific Avenue", color, rent, rentHouses, houseCost, cost);
        addPropertyToMap(32, "North Carolina Avenue", color, rent, rentHouses, houseCost, cost);

        rentHouses = new int[] {150, 450, 1000, 1200, 1400};
        addPropertyToMap(34, "Pennsylvania Avenue", color, rent+2, rentHouses, houseCost, cost + 20);
    }

    private void createDarkBlueProperty(int cost, int rent, int houseCost, PropertyColor color){
        int rentHouses [] = {175, 500, 1100, 1300, 1500};
        addPropertyToMap(37, "Park Place", color, rent, rentHouses, houseCost, cost);

        rentHouses = new int[] {200, 600, 1400, 1700, 2000};
        addPropertyToMap(39, "Boardwalk", color, rent+15, rentHouses, houseCost, cost);
    }

    private void addPropertyToMap(int cellNum, String name, PropertyColor color, int rent, int [] rentHouses, int costPerHouse, int cost){
        Cell cell = new PropertyCell(cellNum, name, color, rent, rentHouses, costPerHouse, cost);
        addCell(cell);
    }

    private void createUtilityAndRailroadsCells(){
        Cell readingRailroad = new RailroadCell(5, "Reading");
        Cell pennsylvaniaRailroad = new RailroadCell(15, "Pennsylvania");
        Cell bandoRailroad = new RailroadCell(25, "B&O");
        Cell shortlineRailroad = new RailroadCell(35, "Shortline");

        Cell electricCompany = new UtilityCell(12, "Electric Company");
        Cell waterworks = new UtilityCell(28, "Water Works");

        addCell(readingRailroad);
        addCell(pennsylvaniaRailroad);
        addCell(bandoRailroad);
        addCell(shortlineRailroad);

        addCell(electricCompany);
        addCell(waterworks);
    }

    private void createChanceAndCommunityCells(){
        Cell chance1 = new CardCell(7, CellType.CHANCE, CellState.ACTION, "Chance");
        Cell chance2 = new CardCell(22, CellType.CHANCE, CellState.ACTION, "Chance");
        Cell chance3 = new CardCell(36, CellType.CHANCE, CellState.ACTION, "Chance");

        addCell(chance1);
        addCell(chance2);
        addCell(chance3);


        Cell community1 = new CardCell(2, CellType.COMMUNITY, CellState.ACTION, "Community Chest");
        Cell community2 = new CardCell(17, CellType.COMMUNITY, CellState.ACTION, "Community Chest");
        Cell community3 = new CardCell(33, CellType.COMMUNITY, CellState.ACTION, "Community Chest");

        addCell(community1);
        addCell(community2);
        addCell(community3);
    }

    private void createFeeCells(){
        Cell go = new FeeCell(0, CellType.GO, CellState.PAYABLE, "GO", 200);
        Cell incomeTax = new FeeCell(4, CellType.FEE, CellState.PAYABLE, "Income Tax", 200);
        Cell luxuryTax = new FeeCell(38, CellType.FEE, CellState.PAYABLE, "Luxury Tax", 100);

        addCell(go);
        addCell(incomeTax);
        addCell(luxuryTax);
    }

    private void createOtherCells(){
        Cell jail = new Cell(10, CellType.JAIL, CellState.ACTION, "Jail");
        Cell goToJail = new Cell(30, CellType.GOJAIL, CellState.ACTION, "Go To Jail");
        Cell freeParking = new Cell(20, CellType.FREE_PARKING, CellState.ACTION, "Free Parking");

        addCell(jail);
        addCell(goToJail);
        addCell(freeParking);
    }


    public void listCells(){
        for (int i = 0; i < 40; i++) {
            System.out.println(cellMap.get(i).toString());
        }
    }

    public Cell getCellAt(int position){
        return cellMap.get(position);
    }
}
