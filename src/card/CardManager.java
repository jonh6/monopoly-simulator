package card;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;

public class CardManager {

    private ArrayList<Card> chanceCardsList = new ArrayList<Card>();
    private ArrayList<Card> communityChestCardsList = new ArrayList<>();

    private LinkedList<Card> chanceCardsQueue = new LinkedList<>();
    private LinkedList<Card> communityChestQueue = new LinkedList<>();

    public CardManager(){
        createChanceCards();
        createCommunityChestCards();
    }


    private void createChanceCards(){
        chanceCardsList.add(new Card(CardAction.MOVE, "Advance to Go", 0));
        chanceCardsList.add(new Card(CardAction.MOVE, "Advance to Illinois", 0));
        chanceCardsList.add(new Card(CardAction.MOVE, "Advance to St. Charles Place", 0));
        chanceCardsList.add(new Card(CardAction.MOVE_PAY, "Advance to the Nearest Utility", 0));

        shuffleChanceCards();
        shuffleCommunityChestCards();
    }

    private void createCommunityChestCards(){

    }



    private LinkedList<Card> shuffle(ArrayList<Card> list){
        ArrayList <Card> listCopy = (ArrayList<Card>) list.clone();
        Collections.shuffle(listCopy);

        return new LinkedList<>(listCopy);
    }

    private void shuffleChanceCards(){
        chanceCardsQueue = shuffle(chanceCardsList);
    }

    private void shuffleCommunityChestCards(){
        communityChestQueue = shuffle(communityChestCardsList);
    }


    public Card getNextChanceCard(){
        if (chanceCardsQueue.isEmpty()){
            shuffleChanceCards();
            System.out.println("Chance Cards Shuffled");
        }

        return chanceCardsQueue.removeFirst();
    }

    public Card getNextCommunityChestCard(){
        if (communityChestQueue.isEmpty()){
            shuffleCommunityChestCards();
        }

        return communityChestQueue.removeFirst();
    }
}
