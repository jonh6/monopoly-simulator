package card;

public class Card {
    private CardAction cardAction;
    private String description;
    private int fee = 0;

    public Card(CardAction cardAction, String description, int fee) {
        this.cardAction = cardAction;
        this.description = description;

        this.fee = fee;
    }

    @Override
    public String toString(){
        return "Type: " + cardAction + " Description: " + description + " Fee:" + fee;
    }
}
