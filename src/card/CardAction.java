package card;

public enum CardAction {
    EARN_FROM_PLAYER(0),
    EARN_FROM_BANK(1),
    PAY_PLAYER(2),
    PAY_BANK(3),
    MOVE(4),
    MOVE_PAY(5),
    GO_JAIL(6),
    GET_OUT_JAIL(7);


    private int type;
    CardAction(int type){
        this.type = type;
    }
}
