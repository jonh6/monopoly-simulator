package cell;

public class FeeCell extends Cell {

    private int fee;

    public FeeCell(int cellNum, CellType cellType, CellState cellState, String name, int fee) {
        super(cellNum, cellType, cellState, name);
        this.fee = fee;
    }

    public int getFee() {
        return fee;
    }

    @Override
    public String toString(){
        return super.toString() + ", Fee:" + fee;
    }
}
