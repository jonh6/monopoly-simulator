package cell;

import player.PlayerNum;

public abstract class OtherPropertyCell extends Cell {
    private PlayerNum owner = PlayerNum.BANK;

    private int cost;
    private boolean ownsAll = false;
    private int numOwned;


    public OtherPropertyCell(int cellNum, CellType cellType, CellState cellState, String name, int cost) {
        super(cellNum, cellType, cellState, name);
        this.cost = cost;
    }


    public int getCost() {
        return cost;
    }

    @Override
    public String toString(){
        return super.toString() + ", Cost:" + cost;
    }

    public boolean isOwnsAll() {
        return ownsAll;
    }

    public int getNumOwned() {
        return numOwned;
    }

    public PlayerNum getOwner(){
        return owner;
    }
}
