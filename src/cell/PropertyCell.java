package cell;

import player.PlayerNum;

import java.util.ArrayList;
import java.util.Arrays;

public class PropertyCell extends Cell {
    private PropertyColor color;
    private int cost;
    private int costPerHouse;

    private int rent;
    private int rentColorSet;
    private int [] rentHouses;

    private PlayerNum owner = PlayerNum.BANK;


    public PropertyCell(int cellNum, String name, PropertyColor color, int rent, int [] rentHouses, int costPerHouse, int cost) {
        super(cellNum, CellType.PROPERTY, CellState.BUYABLE, name);
        this.color = color;
        this.rent = rent;
        this.rentColorSet = rent*2;
        this.costPerHouse = costPerHouse;
        this.cost = cost;
        this.rentHouses = rentHouses;
    }

    public PropertyColor getColor() {
        return color;
    }

    public int getCost() {
        return cost;
    }

    public int getCostPerHouse() {
        return costPerHouse;
    }

    public int getRent() {
        return rent;
    }

    public int getRentColorSet() {
        return rentColorSet;
    }

    public int[] getRentHouses() {
        return rentHouses;
    }

    public PlayerNum getOwner() {
        return owner;
    }

    @Override
    public String toString(){

        return super.toString() + ", Cost:" + cost + ", Rent:" + rent +
                ", Color:" + color + ", HouseCost:" + costPerHouse +
                ", Houses Rent:" + Arrays.toString(rentHouses);
    }
}
