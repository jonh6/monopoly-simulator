package cell;

public class Cell {
    private int cellNum;
    private CellType cellType;
    private CellState cellState;
    private String name;

    public Cell(int cellNum, CellType cellType, CellState cellState, String name) {
        this.cellNum = cellNum;
        this.cellType = cellType;
        this.cellState = cellState;
        this.name = name;
    }

    public int getCellNum() {
        return cellNum;
    }

    public CellType getCellType() {
        return cellType;
    }

    public void setCellType(CellType cellType) {
        this.cellType = cellType;
    }

    public CellState getCellState() {
        return cellState;
    }

    public void setCellState(CellState cellState) {
        this.cellState = cellState;
    }

    public String getName(){
        return name;
    }

    @Override
    public String toString(){
        return "CellNum: " + cellNum + ", Name: " + name;
    }
}
