package cell;

public class UtilityCell extends OtherPropertyCell {
    public UtilityCell(int cellNum, String name) {
        super(cellNum, CellType.OTHER_PROPERTY, CellState.BUYABLE, name, 150);
    }
}
