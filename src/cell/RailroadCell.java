package cell;

public class RailroadCell extends OtherPropertyCell {
    public RailroadCell(int cellNum, String name) {
        super(cellNum, CellType.OTHER_PROPERTY, CellState.BUYABLE, name + " Railroad", 200);
    }
}
