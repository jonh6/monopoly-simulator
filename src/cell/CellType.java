package cell;

public enum CellType {
    PROPERTY(0),
    CHANCE(1),
    COMMUNITY(2),
    JAIL(3),
    FEE(4),
    GO(5),
    GOJAIL(6),
    OTHER_PROPERTY(7),
    FREE_PARKING(8);


    private int type;
    CellType(int type) {
        this.type = type;
    }
}
