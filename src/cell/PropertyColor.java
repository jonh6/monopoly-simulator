package cell;

public enum PropertyColor {
    BROWN(0),
    LIGHT_BLUE(1),
    PINK(2),
    ORANGE(3),
    RED(4),
    YELLOW(5),
    GREEN(6),
    DARK_BLUE(7);

    private int color;

    PropertyColor(int color) {
        this.color = color;
    }
}
