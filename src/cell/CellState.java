package cell;

public enum CellState {
    BUYABLE(0),
    ACTION(1),
    PAYABLE(2);


    private int state;

    CellState(int state) {
        this.state = state;
    }
}
